package com.brainacad.oop.testnest1;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by andriy on 15.03.17.
 */
public class Lab3 extends JFrame {
    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            JMenu jMenuFile = new JMenu("File");
            JMenu jMenu = new JMenu("Help");
            JMenuItem jMenuItem = new JMenuItem("about");
            jMenuItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    JOptionPane.showMessageDialog(null,"Hello this is help");
                }
            });
            jMenu.add(jMenuItem);
            jMenu.addSeparator();
            JMenuItem jMenuItem2 = new JMenuItem("about2");
            jMenu.add(jMenuItem2);

            JMenuBar jMenuBar = new JMenuBar();
            jMenuBar.add(jMenuFile);
            jMenuBar.add(jMenu);
            JFrame frame = new JFrame();
            frame.setJMenuBar(jMenuBar);

            frame.setSize(400, 300);
            frame.setVisible(true);
            frame.setTitle("Executor");
            Container container = frame.getContentPane();
            container.setLayout(new BorderLayout());
            JLabel label = new JLabel("<html><b>Run a program </b></html>");
            List<String> list = new ArrayList<>();

            JComboBox<String> jComboBox = new JComboBox();
            jComboBox.addItem("Calculator");
            jComboBox.addItem("LeafPad");

            JButton jButton = new JButton("RUN!!!");
            jButton.setToolTipText("натисни мене");

            JPanel jPanel = new JPanel();
            jPanel.add(label);
            jPanel.add(jComboBox);
            jPanel.add(jButton);
            frame.add(jPanel, BorderLayout.NORTH);
            frame.add(new JButton("Left"), BorderLayout.WEST);
            frame.add(new JButton("Right"), BorderLayout.EAST);
            frame.add(new JButton("Down"), BorderLayout.SOUTH);
            frame.add(new JTextArea("fljsdaflkjadsflkjadflkjasdflkjadsf"), BorderLayout.CENTER);
            class MyHandler implements ActionListener{

                public void actionPerformed(ActionEvent e){
                    String name = (String)jComboBox.getSelectedItem();
                    System.out.println(e.toString());
                    try {
                        switch (name) {
                            case "LeafPad": {
                                Runtime.getRuntime().exec("/usr/bin/leafpad");
                                break;
                            }
                            case "Calculator":{
                                Runtime.getRuntime().exec("/usr/bin/evince");
                                break;
                            }
                            default:{
                                System.out.println("this comm not found");
                            }
                        }
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
            }
            jButton.addActionListener(new MyHandler());
        });



    }
}
