package com.brainacad.oop.testnest1;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.image.BufferedImage;

/**
 * Created by andriy on 3/21/17.
 */
public class WindowOperationsDemo extends JFrame {


    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            JFrame jFrame = new WindowOperationsDemo();
            SystemTray systemTray = SystemTray.getSystemTray();
//            TrayIcon trayIcon = new TrayIcon(new Image("images/smile.png")," click on me");
;
            jFrame.addWindowListener(new WindowListener() {
                @Override
                public void windowOpened(WindowEvent e) {
                    JOptionPane.showMessageDialog(null,"Hello i am opened");
                }

                @Override
                public void windowClosing(WindowEvent e) {
                    JOptionPane.showMessageDialog(null,"Hello i will be closed");
                    jFrame.dispose();
                    System.exit(0);
                }

                @Override
                public void windowClosed(WindowEvent e) {
                    JOptionPane.showMessageDialog(null,"I was disposed");
                }

                @Override
                public void windowIconified(WindowEvent e) {
                    if (!SystemTray.isSupported()) {
                     System.out.println("SystemTray is not supported");
                       return;
                    }


                    JOptionPane.showMessageDialog(null,"I go to system tray");
                }

                @Override
                public void windowDeiconified(WindowEvent e) {
                    JOptionPane.showMessageDialog(null,"I go to foreground");
                }

                @Override
                public void windowActivated(WindowEvent e) {
                    JOptionPane.showMessageDialog(null,"Alt Tab and i m here");
                }

                @Override
                public void windowDeactivated(WindowEvent e) {
                    JOptionPane.showMessageDialog(null,"Alt Tab and i m not here");
                }
            });
            jFrame.setTitle("Window demo");
            Toolkit toolkit = Toolkit.getDefaultToolkit();
            Dimension screenSize = toolkit.getScreenSize();
            jFrame.setSize(screenSize.width / 2, screenSize.height / 2);
            jFrame.setVisible(true);
        });
    }




}
