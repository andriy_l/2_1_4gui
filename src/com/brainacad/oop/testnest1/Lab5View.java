package com.brainacad.oop.testnest1;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Created by andriy on 18.03.17.
 */
public class Lab5View extends JFrame implements WindowListener{
    private JPanel userNamePanel;
    private JPanel hostNamePanel;
    private JPanel resultPanel;

    private JTextField userNameTextField;
    private JTextField resultTextField;
    private JTextField hostNameTextField;
    private JButton userNameButton;
    private JButton hostNameButton;
    private JMenuBar jMenuBar;
    private JFrame mainFrame;



    private void createMenu(){
        jMenuBar = new JMenuBar();

        JMenu jMenuMessage = new JMenu("Message");
        JMenuItem jMenuItemQuestion = new JMenuItem("Question");
        JMenuItem jMenuItemInputName = new JMenuItem("Input Name");
        jMenuMessage.add(jMenuItemQuestion);
        jMenuMessage.add(jMenuItemInputName);

        jMenuItemQuestion.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int result =
                        JOptionPane.showConfirmDialog(null, "You are ready to undergo the test?", "Question Title", JOptionPane.YES_NO_OPTION);
                resultTextField.setText(result+"");
            }
        });

        jMenuItemInputName.addActionListener((e) -> {
            String str = JOptionPane.showInputDialog(this,"Input your name: ");
            resultTextField.setText(resultTextField.getText()+str);
            System.out.println("command"+ e.getActionCommand());
            System.out.println("params"+ e.paramString());
            }
        );

        JMenu jMenuHelp = new JMenu("Help");
        JMenuItem jMenuItemFAQ = new JMenuItem("FAQ");
        JMenuItem jMenuItemHelp = new JMenuItem("HELP");
        jMenuHelp.add(jMenuItemFAQ);
        jMenuHelp.addSeparator();
        jMenuHelp.add(jMenuItemHelp);

        jMenuBar.add(jMenuMessage);
        jMenuBar.add(jMenuHelp);

    }

    private void createUserNamePanel(){
        userNamePanel = new JPanel();
        userNamePanel.setLayout(new FlowLayout(FlowLayout.CENTER, 20, 20));
        userNamePanel.add(new JLabel(" "));
        userNameTextField = new JTextField("here will be smth");
        userNamePanel.add(userNameTextField);
        userNameButton = new JButton("GET USER NAME");
        userNamePanel.add(userNameButton);

        userNameButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                userNameTextField.setText(System.getProperty("user.name"));
                userNameButton.setEnabled(false);
            }
        });

    }

    private void createResultPanel(){
        resultPanel = new JPanel();
        resultPanel.add(new JLabel("Result:"));
        resultTextField = new JTextField(20);
        resultPanel.add(resultTextField);
    }

    private void createHostNamePanel(){
        hostNamePanel = new JPanel();
        hostNamePanel.setLayout(new FlowLayout(FlowLayout.CENTER, 20, 20));
        hostNamePanel.add(new JLabel(" "));
        hostNameTextField = new JTextField("here will be smth");
        hostNamePanel.add(hostNameTextField);
        hostNameButton = new JButton("GET HOST NAME");
        hostNamePanel.add(hostNameButton);

        hostNameButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    hostNameTextField.setText(InetAddress.getLocalHost().getHostName());
                } catch (UnknownHostException e1) {
                    e1.printStackTrace();
                }
                hostNameButton.setEnabled(false);
            }
        });

    }

    private void setFrame(){
        createMenu();
        mainFrame.setJMenuBar(jMenuBar);
                Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension screenSize = toolkit.getScreenSize();
        mainFrame.setSize(screenSize.width/2, screenSize.height/2);
        Container container = mainFrame.getContentPane();
        container.setLayout(new BorderLayout());
        createHostNamePanel();
        createResultPanel();
        createUserNamePanel();
        mainFrame.add(userNamePanel, BorderLayout.NORTH);
        mainFrame.add(resultPanel, BorderLayout.CENTER);
        mainFrame.add(hostNamePanel, BorderLayout.SOUTH);
        mainFrame.setTitle("My Application");
        mainFrame.setVisible(true);
        mainFrame.addWindowListener(this);

    }

    public Lab5View(JFrame frame){
        this.mainFrame = frame;
        setFrame();
    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {
        int result = JOptionPane.showConfirmDialog(null,"Really close?","Title of really close", JOptionPane.YES_NO_OPTION);
        if(result == JOptionPane.YES_OPTION) {
            dispose();
            System.exit(0);
        } else {
            System.out.println("im not close");
            mainFrame.setVisible(true);
            mainFrame.repaint();
        }
    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }

    
}
