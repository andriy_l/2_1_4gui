package samples;

/**
 * Created by andriy on 18.03.17.
 */
public class ConverterModel {
    private String userName;
    private String hostName;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }
}
