package samples;

import javax.swing.*;

/**
 * Created by andriy on 18.03.17.
 */
public class Client {
    public static void main(String[] args) {
        ConverterView frame = new ConverterView("Converter");
        frame.setTitle("Converter");
        frame.setSize(900,400);
        frame.add(frame.getMainPanel());

        frame.setVisible(true);

    }
}
