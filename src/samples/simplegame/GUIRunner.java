package samples.simplegame;

import javax.swing.*;
import java.awt.*;

/**
 * Created by andriy on 7/10/17.
 */
public class GUIRunner {
    public static void main(String[] args) {

        // GUI має стартувати в окремому потоці,
        EventQueue.invokeLater(() -> {
            MyFrame myFrame = new MyFrame();
            myFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

            myFrame.setSize(300,300);
            myFrame.setVisible(true);
        });


    }
}
