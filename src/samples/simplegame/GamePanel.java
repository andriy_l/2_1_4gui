package samples.simplegame;

import javax.swing.*;
import java.awt.*;
import java.util.Random;

/**
 * Created by andriy on 7/11/17.
 */
public class GamePanel extends JPanel implements Runnable{
    Dimension dimension;
    double height;
    double width;
    Random random;

    @Override
    public void run() {
        dimension = super.getSize();
        height = dimension.getHeight();
        width = dimension.getWidth();
        setLayout(new GridLayout(4, 4, 2, 2));
        random = new Random();
        int value = 0;
        setBackground(Color.BLUE);
        JPanel[] labels = new JPanel[4 * 4];
        for (int i = 0; i < (4 * 4); i++) {
            labels[i] = getSquarePanel();
            labels[i].setVisible(false);
            add(labels[i]);
        }



        while (true) {

            value = random.nextInt(16);
            System.out.println(value);
            JButton colobok = null;
            try {
                labels[value].setVisible(true);

                colobok = new JButton( String.valueOf('\u263A') );
                colobok.setSize( (int)(width/4),(int) (height/4) );
                colobok.addActionListener(e -> {
                    System.out.println("catched");


                });

                labels[value].add( colobok );
            }catch (ArrayIndexOutOfBoundsException a){
                System.out.println(a);
            }
            try {
                Thread.sleep(1_000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            labels[value].remove(colobok);
            labels[value].setVisible(false);
        }
    }
    private JPanel getSquarePanel(){
        JPanel jLabel = new JPanel();
        jLabel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        return jLabel;
    }
}
