package samples.simplegame;

import javax.swing.*;
import java.awt.event.ActionEvent;

/**
 * Created by andriy on 7/10/17.
 */
public class MyFrame extends JFrame {
    JMenuBar menuBar;
    JMenu menu;
    JMenu menuHelp;
    JMenuItem startMenuItem;
    JMenuItem itEdit;
    JMenuItem find;

    GamePanel gamePanel;


    MyFrame() {
        setResizable(false);
        gamePanel = new GamePanel();
        Thread thread = new Thread(gamePanel);
        startMenuItem = new JMenuItem("Start");
        startMenuItem.addActionListener(e -> thread.start());
        itEdit = new JMenuItem("Exit");
        itEdit.addActionListener((e -> System.exit(0)));
        menuBar = new JMenuBar();
        menu = new JMenu("Hello");
        menuHelp = new JMenu("Help");
        menu.add(startMenuItem);
        menu.add(itEdit);
        JMenuItem aboutItem = new JMenuItem("About");
        aboutItem.addActionListener((ActionEvent event) -> {
            JOptionPane.showMessageDialog(null, "Press Start to Begin", "Help Option Pane", JOptionPane.INFORMATION_MESSAGE);
        });
        menuHelp.add(aboutItem);

        menuBar.add(menu);
        menuBar.add(menuHelp);
        setJMenuBar(menuBar);

        add(gamePanel);

    }




}
