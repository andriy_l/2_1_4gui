package samples;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Created by andriy on 18.03.17.
 */
public class ConverterView extends JFrame implements WindowListener{

    private JPanel mainPanel;
    private JLabel progNameLabel;
    private JTextField userNameTextField;
    private JButton get_User_NameButton;
    private JLabel jLabel2;
    private JTextField hostNameTextField;
    private JButton get_Host_NameButton2;
    ConverterModel converterModel;

    public JPanel getMainPanel(){
        return mainPanel;
    }


    public ConverterView(String string) {

        get_User_NameButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                userNameTextField.setText(System.getProperties().getProperty("user.name"));

            }
        });

        get_Host_NameButton2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    hostNameTextField.setText(InetAddress.getLocalHost().getHostName());
                } catch (UnknownHostException e1) {
                    e1.printStackTrace();
                }
            }
        });
    }



    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {

    }

    @Override
    public void windowClosed(WindowEvent e) {
        dispose();
        System.exit(0);
    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }
}
