import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class MyBaseNotePad extends JFrame {
    JTextArea textArea;
    {
        EventQueue.invokeLater(() -> {




                    Toolkit toolkit = Toolkit.getDefaultToolkit();
                    toolkit.beep();
                    Dimension screenSize = toolkit.getScreenSize();

                    setSize(screenSize.width / 2, screenSize.height / 2);
                    setTitle("MyChildNotePad");

                    JMenuBar menuBar = new JMenuBar();
                    JMenu fileMenu = new JMenu("File");
                    textArea = new JTextArea();
                    textArea.addKeyListener(new KeyListener() {
                        @Override
                        public void keyTyped(KeyEvent e) {
                            toolkit.beep();
                        }

                        @Override
                        public void keyPressed(KeyEvent e) {
                            toolkit.beep();
                        }

                        @Override
                        public void keyReleased(KeyEvent e) {
                            toolkit.beep();
                        }
                    });
                    textArea.setEnabled(true);
                    textArea.setEditable(true);
                    textArea.setBackground(Color.BLACK);
                    textArea.setForeground(Color.GREEN);

                    ActionListener actionListener = new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {

                            MyChildNotePad myChildNotePad = new MyChildNotePad(textArea);
                            myChildNotePad.runChildNotePad();
                        }
                    };


                    JMenuItem openFile = new JMenuItem("Open...");
                    openFile.addActionListener(actionListener);
                    JMenuItem saveFile = new JMenuItem("Save...");
                    saveFile.addActionListener(e -> {

                    });
                    JMenuItem exitFile = new JMenuItem("Exit");
                    exitFile.addActionListener(e ->
                            {
                                int value =
                                        JOptionPane.showConfirmDialog(null,
                                                "Ви справді бажаєте вийти?",
                                                "Really exit?",
                                                JOptionPane.YES_NO_OPTION);
                                System.out.println(value);
                                if (value == 0) {
                                    System.exit(0);
                                }
                            }
                    );
                    fileMenu.add(openFile);
                    fileMenu.add(saveFile);
                    fileMenu.addSeparator();
                    fileMenu.add(exitFile);

                    JMenu helpMenu = new JMenu("Help");
                    JMenuItem helpItem = new JMenuItem("Help");
                    helpItem.addActionListener(e ->{
                        JFrame jFrame1 = new JFrame("Help to notepad");
                        jFrame1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                        jFrame1.setVisible(true);
                        jFrame1.setSize(400, 400);
                        Container c = jFrame1.getContentPane();
                        JEditorPane editorPane = new JEditorPane();
                        JScrollPane jScrollPane = new JScrollPane(editorPane);
                        try {
                            editorPane.setPage("http://ukr.net");
                        } catch (IOException e1) {
                            e1.printStackTrace();
                        }
                        editorPane.setEditable(false);
                    });
                    JMenuItem aboutItem = new JMenuItem("About");
                    // add behavior
                    aboutItem.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            JOptionPane.showMessageDialog(null,
                                    "Best JAVA developer",
                                    "Про автора програми",
                                    JOptionPane.OK_OPTION);
                        }
                    });

                    helpMenu.add(helpItem);
                    helpMenu.addSeparator();
                    helpMenu.add(aboutItem);
                    menuBar.add(fileMenu);
                    menuBar.add(helpMenu);
                    setJMenuBar(menuBar);

                    Container container = getContentPane();
                    JPanel mainPanel = new JPanel();
                    mainPanel.setLayout(new GridLayout(4, 4));
                    mainPanel.setLayout(new BorderLayout());

                    JPanel jPanel = new JPanel();


                    JScrollPane jScrollPane = new JScrollPane(textArea);


                    container.add(jScrollPane);

                    setVisible(true);
                    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                }
        );
    }
    public void setText(String str) {
        textArea.setText(str);
    }
    public static void main(String[] args) {
        MyBaseNotePad jFrame = new MyBaseNotePad();

    }

}
