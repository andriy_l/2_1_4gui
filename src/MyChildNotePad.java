import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class MyChildNotePad {
    private JTextArea myBaseNotePad;

    public MyChildNotePad(JTextArea myBaseNotePad) {
        this.myBaseNotePad = myBaseNotePad;
    }

    public void runChildNotePad() {
        EventQueue.invokeLater(() -> {

            JFrame jFrame = new JFrame();
            Toolkit toolkit = Toolkit.getDefaultToolkit();
            toolkit.beep();
            Dimension screenSize = toolkit.getScreenSize();

            jFrame.setSize(screenSize.width / 2, screenSize.height / 2);
            jFrame.setTitle("MyChildNotePad");

            JMenuBar menuBar = new JMenuBar();
            JMenu fileMenu = new JMenu("File");
            JTextArea textArea = new JTextArea();
            textArea.addKeyListener(new KeyListener() {
                @Override
                public void keyTyped(KeyEvent e) {

                    myBaseNotePad.setText(textArea.getText());
                }

                @Override
                public void keyPressed(KeyEvent e) {
                    myBaseNotePad.setText(textArea.getText());
                }

                @Override
                public void keyReleased(KeyEvent e) {
                    toolkit.beep();
                }
            });
            textArea.setEnabled(true);
            textArea.setEditable(true);
            textArea.setBackground(Color.BLACK);
            textArea.setForeground(Color.GREEN);

            ActionListener actionListener = new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {

                    JFileChooser fileChooser = new JFileChooser();
                    FileNameExtensionFilter filter = new FileNameExtensionFilter(
                            "Text Files", "txt", "csv", "info", "MD");
                    fileChooser.setFileFilter(filter);
                    int returnVal = fileChooser.showOpenDialog(null);
                    if (returnVal == JFileChooser.APPROVE_OPTION) {
                        File file = fileChooser.getSelectedFile();
                        System.out.println("You choose to open this file: " +
                                file);
                        try {
                            String string = new String(Files.readAllBytes(file.toPath()));
                            textArea.setText(string);
                            toolkit.beep();
                        } catch (IOException e1) {
                            e1.printStackTrace();
                        }
                    }
                }
            };


            JMenuItem openFile = new JMenuItem("Open...");
            openFile.addActionListener(actionListener);
            JMenuItem saveFile = new JMenuItem("Save...");
            saveFile.addActionListener(e -> {
                JFileChooser fileSaveChooser = new JFileChooser();
                FileNameExtensionFilter filter = new FileNameExtensionFilter(
                        "Text Files", "txt", "csv", "info", "MD");
                fileSaveChooser.setFileFilter(filter);
                int returnVal = fileSaveChooser.showSaveDialog(null);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    File file = fileSaveChooser.getSelectedFile();
                    byte[] editedTextAsBytes = textArea.getText().getBytes();
                    try {
                        Files.write(file.toPath(), editedTextAsBytes);
                        toolkit.beep();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
            });
            JMenuItem exitFile = new JMenuItem("Exit");
            exitFile.addActionListener(e ->
                    {
                        int value =
                                JOptionPane.showConfirmDialog(null,
                                        "Ви справді бажаєте вийти?",
                                        "Really exit?",
                                        JOptionPane.YES_NO_OPTION);
                        System.out.println(value);
                        if (value == 0) {
                            System.exit(0);
                        }
                    }
            );
            fileMenu.add(openFile);
            fileMenu.add(saveFile);
            fileMenu.addSeparator();
            fileMenu.add(exitFile);

            JMenu helpMenu = new JMenu("Help");
            JMenuItem helpItem = new JMenuItem("Help");
            helpItem.addActionListener(e ->{
                JFrame jFrame1 = new JFrame("Help to notepad");
                jFrame1.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                jFrame1.setVisible(true);
                jFrame1.setSize(400, 400);
                Container c = jFrame1.getContentPane();
                JEditorPane editorPane = new JEditorPane();
                JScrollPane jScrollPane = new JScrollPane(editorPane);
                try {
                    editorPane.setPage("http://ukr.net");
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                editorPane.setEditable(false);
            });
            JMenuItem aboutItem = new JMenuItem("About");
            // add behavior
            aboutItem.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    JOptionPane.showMessageDialog(null,
                            "Best JAVA developer",
                            "Про автора програми",
                            JOptionPane.OK_OPTION);
                }
            });

            helpMenu.add(helpItem);
            helpMenu.addSeparator();
            helpMenu.add(aboutItem);
            menuBar.add(fileMenu);
            menuBar.add(helpMenu);
//            jFrame.setJMenuBar(menuBar);
            JPanel jPanel = new JPanel();

            Container container = jFrame.getContentPane();
            jPanel.setLayout(new GridLayout(1,2));
            JButton send = new JButton("send");

            JPanel mainPanel = new JPanel();
            jPanel.add(send);
            jPanel.add(mainPanel);

            JScrollPane jScrollPane = new JScrollPane(textArea);


            container.add(jScrollPane);

            jFrame.setVisible(true);
            jFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        }
        );
    }

}
